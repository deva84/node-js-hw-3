require('dotenv').config();
const jwt = require('jsonwebtoken');
const { User } = require('../models/userModel');
const JWT_SECRET = process.env.JWT_SECRET || 'yahoo';

const auth = async (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res
        .status(400)
        .json({message: `No Authorization http header found!`});
  }
  const [, token] = header.split(' ');
  if (!token) {
    return res.status(400).json({message: `No JWT token found!`});
  }

  if (!token) {
    return res
        .status(400)
        .json({message: `No Authorization http header found!`});
  }

  const decoded = jwt.verify(token, JWT_SECRET);

  const user = await User.findOne({
    '_id': decoded._id,
    'tokens.token': token,
  });

  if (!user) {
    res.status(400).json({message: 'You should login'});
  }
  req.token = token;
  req.user = user;
  next();
};

module.exports = auth;
