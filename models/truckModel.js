const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema(
    {
      created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
      },
      assigned_to: {
        type: mongoose.Schema.Types.ObjectId,
        default: null,
        ref: 'User',
      },
      type: {
        type: String,
        required: true,
        trim: true,
      },
      status: {
        type: String,
        default: 'IS',
        trim: true,
      },
      created_date: {
        type: Date,
        default: Date.now(),
      },
    },
    {
      collection: 'trucks',
    },
);

truckSchema.methods.toJSON = function() {
  const truck = this;
  const truckObject = truck.toObject();

  delete truckObject.__v;
  truckObject.created_by.toString();

  return truckObject;
};

module.exports.Truck = mongoose.model('Truck', truckSchema);
