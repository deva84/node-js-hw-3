const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema(
    {
      created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
      },
      assigned_to: {
        type: mongoose.Schema.Types.ObjectId,
        default: null,
        ref: 'User',
      },
      state: {
        type: String,
        default: 'En route to Pick Up',
      },
      status: {
        type: String,
        default: 'NEW',
        trim: true,
      },
      name: {
        type: String,
        required: true,
        trim: true,
      },
      payload: {
        type: Number,
        required: true,
      },
      pickup_address: {
        type: String,
        required: true,
        trim: true,
      },
      delivery_address: {
        type: String,
        required: true,
        trim: true,
      },
      dimensions: {
        width: {
          type: Number,
          required: true,
        },
        length: {
          type: Number,
          required: true,
        },
        height: {
          type: Number,
          required: true,
        },
      },
      created_date: {
        type: Date,
        default: Date.now(),
      },
      logs: [
        {
          message: {
            type: String,
          },
          time: {
            type: Date,
            default: Date.now(),
          },
        },
      ],
    },
    {
      collection: 'loads',
    },
);

loadSchema.methods.toJSON = function() {
  const load = this;
  const loadObject = load.toObject();

  delete loadObject.__v;
  loadObject.logs.forEach((log) => {
    delete log._id;
  });
  loadObject.created_by.toString();

  return loadObject;
};

module.exports.Load = mongoose.model('Load', loadSchema);
