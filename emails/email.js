require('dotenv').config();
const sgMail = require('@sendgrid/mail');
const KEY = process.env.KEY || 8080;

sgMail.setApiKey(KEY);

const mailWithPass = (email) => {
  sgMail.send({
    to: email,
    from: 'svitlana_dasgupta@epam.com',
    subject: 'New password',
    text: 'Dear user, your new password is "newPassword',
  });
};

module.exports = { mailWithPass };
