require('dotenv').config();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET || 'yahoo';
const { User } = require('../models/userModel');
const { mailWithPass } = require('../emails/email');

const registration = async (req, res) => {
    const { email, password, role } = req.body;

    if (email && password && role) {
        let user = await User.findOne({ email });
        if (user) {
            res.status(400).json({ message: 'User is already exist!' });
        } else {
            user = new User({
                email,
                password: await bcrypt.hash(password, 10),
                role,
            });
            const token = jwt.sign({ email: user.email, _id: user._id }, JWT_SECRET);

            user.tokens = user.tokens.concat({ token });

            try {
                await user.save();
            } catch (e) {
                res.status(400).json({ message: e.message });
            }

            res.status(200).json({ message: 'Profile has been created' });
        }
    } else {
        res.status(400).json({ message: 'All fields are compulsory' });
    }
};

const login = async (req, res) => {
    const { email, password } = req.body;

    const user = await User.findOne({ email });
    if (!email && !password) {
        res.status(400).json({ message: 'All fields are compulsory' });
    }

    if (!user) {
        return res
            .status(400)
            .json({ message: `No user with email '${email}' found` });
    }

    if (!(await bcrypt.compare(password, user.password))) {
        return res.status(400).json({ message: `Wrong password` });
    }

    const token = jwt.sign({ email: user.email, _id: user._id }, JWT_SECRET);

    user.tokens = user.tokens.concat({ token });
    await user.save();
    res.status(200).json({ jwt_token: token });
};

const forgotPassword = async (req, res) => {
    const { email } = req.body;

    if (!email) {
        res.status(400).json({ message: 'All fields are compulsory' });
    }

    const user = await User.findOne({ email });

    if (!user) {
        return res
            .status(400)
            .json({ message: `No user with email '${email}' found` });
    }

    user.password = await bcrypt.hash('newPassword', 10);
    mailWithPass(email);
    await user.save();
    res.status(200).json({ message: 'New password sent to your email address' });
};

module.exports = { registration, login, forgotPassword };