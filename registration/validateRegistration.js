const Joi = require('joi');

const validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email({minDomainSegments: 2, tlds: {allow: true}})
        .required(),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),
    role: Joi.string().pattern(new RegExp('(SHIPPER|DRIVER)$')).required(),
  });

  const result = schema.validate(req.body);
  const {error} = result;
  const valid = error == null;
  if (!valid) {
    res.status(400).json({
      message: 'Invalid data',
    });
  }
  next();
};
 module.exports = {validateRegistration};