const express = require('express');
const router = new express.Router();
const auth = require('../middlewares/auth');


const { Load } = require('../models/loadModel');
const { Truck } = require('../models/truckModel');

router.post('/', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'SHIPPER') {
      const {
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions: { width, length, height }
      } = req.body;
      if (
        name &&
        payload &&
        pickup_address &&
        delivery_address &&
        width &&
        length &&
        height
      ) {
        const load = new Load({
          created_by: req.user._id,
          name,
          payload,
          pickup_address,
          delivery_address,
          dimensions: {
            width,
            length,
            height
          }
        });
        try {
          await load.save();
          res.status(200).json({ message: 'Load has been created' });
        } catch (e) {
          res.status(400).json({ message: e.message });
        }
      } else {
        res.status(400).json({ message: 'All fields are compulsory' });
      }
    } else {
      res.status(400).json({ message: 'You are not shipper!' });
    }
  } else {
    res.status(400).json({ message: 'You should log in' });
  }
});

router.get('/', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'SHIPPER') {
      const match = {};
      if (req.query.status) {
        match.status = JSON.parse(req.query.status);
      }
      try {
        await user
          .populate({
            path: 'loads',
            match,
            options: {
              limit: parseInt(req.query.limit),
              skip: parseInt(req.query.offset)
            }
          })
          .execPopulate();
        res.status(200).json({ loads: [...req.user.loads] });
      } catch (e) {
        res.status(400).json({ message: e.message });
      }
    } else {
      res.status(400).json({ message: 'You are not shipper!' });
    }
  } else {
    res.status(400).json({ message: 'Please autheticate!' });
  }
});

router.get('/active', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'DRIVER') {
      const trucks = await Truck.find({ created_by: user._id });
      const truck = trucks.find((truck) => (truck.assigned_to = user._id));
      const load = await Load.findOne({ assigned_to: truck._id });
      if (!load) {
        return res.status(400).json('You have no active loads');
      }

      res.status(200).json({ load: load });
    } else {
      res.status(400).json({ message: 'You are not driver' });
    }
  } else {
    res.status(400).json({ message: 'You should log in' });
  }
});

router.patch('/active/state', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'DRIVER') {
      const trucks = await Truck.find({ created_by: user._id });
      const truck = trucks.find((truck) => (truck.assigned_to = user._id));
      const load = await Load.findOne({ assigned_to: truck._id });
      if (!load) {
        return res.status(400).json('You have no active loads');
      }
      let message;
      if (load.state === 'En route to delivery') {
        message = 'Arrived to delivery';
        load.status = 'SHIPPED';
        load.state = message;
        load.logs.push({ message: `Load is ${message}`, time: Date.now() });
        load.save();
      } else if (load.state === 'En route to Pick Up') {
        message = 'Arrived to Pick Up';
        load.state = message;
        load.logs.push({ message: `Load is ${message}`, time: Date.now() });
        load.save();
      } else if (load.state === 'Arrived to Pick Up') {
        message = 'En route to delivery';
        load.state = message;
        load.logs.push({ message: `Load is ${message}`, time: Date.now() });
        load.save();
      }
      res.status(200).json({ message: `Load state changed to ${message}` });
    } else {
      res.status(400).json({ message: 'You are not driver' });
    }
  } else {
    res.status(400).json({ message: 'You should log in' });
  }
});

router.get('/:id', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'SHIPPER') {
      try {
        const _id = req.params.id;
        const load = await Load.findOne({ _id, created_by: req.user._id });

        if (!load) {
          return res.status(400).json({ message: 'No such load' });
        }
        res.status(200).json({ load: load });
      } catch (e) {
        res.status(400).json({ message: e.message });
      }
    } else {
      res.status(400).json({ message: 'You are not shipper' });
    }
  } else {
    res.status(400).json({ message: 'You should log in' });
  }
});

router.put('/:id', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'SHIPPER') {
      const updates = [];
      for (const value of Object.keys(req.body)) {
        if (typeof req.body[value] === 'object') {
          const res = Object.keys(req.body[value]);
          updates.push(...res);
        } else {
          updates.push(value);
        }
      }
      const allowedUpdates = [
        'name',
        'payload',
        'pickup_address',
        'delivery_address',
        'width',
        'length',
        'height'
      ];
      const isAllowed = updates.every((update) =>
        allowedUpdates.includes(update)
      );

      if (!isAllowed) {
        return res.status(400).send({ error: 'Invalid data' });
      }

      try {
        const load = await Load.findOne({
          _id: req.params.id,
          created_by: req.user._id
        });
        if (!load) {
          return res.status(400).json({ message: 'No such load' });
        }
        if (load.status === 'NEW') {
          updates.forEach((update) => {
            if (
              update === 'width' ||
              update === 'length' ||
              update === 'height'
            ) {
              load.dimensions[update] = req.body.dimensions[update];
            } else {
              load[update] = req.body[update];
            }
          });
          await load.save();
          res
            .status(200)
            .json({ message: 'Load details were updated' });
        } else {
          res.status(400).json({
            message: 'Try later'
          });
        }
      } catch (e) {
        res.status(400).json({ message: e.message });
      }
    } else {
      res.send(400).json({ message: 'You are not shipper' });
    }
  } else {
    res.status(400).json({ message: 'You should log in' });
  }
});

router.delete('/:id', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'SHIPPER') {
      const load = await Load.findOneAndDelete({
        _id: req.params.id,
        created_by: req.user._id
      });
      if (!load) {
        return res.status(404).json({ message: 'No such load' });
      }
      if (load.status === 'NEW') {
        res.status(200).json({ message: 'Load has been deleted' });
      } else {
        res.status(400).json({
          message: 'Try later'
        });
      }
    } else {
      res.send(400).json({ message: 'You are not shipper' });
    }
  } else {
    res.status(400).json({ message: 'You should log in' });
  }
});

router.post('/:id/post', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'SHIPPER') {
      const _id = req.params.id;
      const load = await Load.findOne({ _id, created_by: user._id });

      if (!load) {
        return res.status(400).json({ message: 'No such load' });
      }
      const trucks = await Truck.find();

      const suitableTruck = trucks.find((truck) => truck.assigned_to !== null);
      if (suitableTruck) {
        load.status = 'ASSIGNED';
        load.assigned_to = suitableTruck._id;
        load.logs.push({
          message: `Load has been assigned to driver  ${suitableTruck.assigned_to}`,
          time: Date.now()
        });
        await load.save();
        res.status(200).json({
          message: 'Load posted successfully',
          driver_found: true
        });
      } else {
        res.status(400).json({ message: 'No suitable truck for you at the moment' });
      }
    } else {
      res.send(400).json({ message: 'You are not shipper' });
    }
  } else {
    res.status(400).json({ message: 'You should log in' });
  }
});

router.get('/:id/shipping_info', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'SHIPPER') {
      const _id = req.params.id;
      const load = await Load.findOne({ _id, created_by: user._id });

      if (!load) {
        return res.status(400).json({ message: 'No such load' });
      }

      const truck = await Truck.findOne({ _id: load.assigned_to });

      res.status(200).json({ load: load, truck: truck });
    } else {
      res.status(400).json({ message: 'You are not shipper' });
    }
  } else {
    res.status(400).json({ message: 'You should log in' });
  }
});

module.exports = router;
