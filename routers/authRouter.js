const express = require('express');
const router = new express.Router();
const { asyncWrapper } = require('../middlewares/helpers');
const { validateRegistration } = require('../registration/validateRegistration');
const { registration, login, forgotPassword } = require('../registration/registration');

router.post('/register', asyncWrapper(validateRegistration), asyncWrapper(registration));
router.post('/login', asyncWrapper(login));
router.post('/forgot_password', asyncWrapper(forgotPassword));

module.exports = router;
