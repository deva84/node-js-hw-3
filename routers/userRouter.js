const express = require('express');
const router = new express.Router();

const auth = require('../middlewares/auth');
const bcrypt = require('bcrypt');

const sharp = require('sharp');
const multer = require('multer');

router.get('/', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    res.status(200).json({user: user});
  } else {
    res.status(400).json({message: 'You should log in'});
  }
});

router.delete('/', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'SHIPPER') {
      try {
        user = [];
        await user.save();
        res.status(200).json({message: 'Success'});
      } catch (e) {
        res.status(400).json({message: e.message});
      }
    } else {
      res.status(400).json({message: 'You can\'t delete account'});
    }
  } else {
    res.status(400).json({message: 'You should log in'});
  }
});

router.patch('/password', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    const {oldPassword, newPassword} = req.body;
    if (oldPassword && newPassword) {
      if (!(await bcrypt.compare(oldPassword, user.password))) {
        return res.status(400).json({message: `Wrong password`});
      } else {
        if (newPassword != oldPassword) {
          user.password = await bcrypt.hash(newPassword, 10);
          await user.save();
          res.status(200).json({message: 'Success'});
        }
      }
    } else {
      res.status(400).json({message: 'All fields are compulsory'});
    }
  } else {
    res.status(400).json({message: 'You should log in'});
  }
});

const upload = multer({
  limits: {
    fileSize: 1200000,
  },
  fileFilter(req, file, callback) {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
      return callback(new Error('This file type is not supported'));
    }
    callback(undefined, true);
  },
});

router.post(
    '/photo',
    auth,
    upload.single('photo'),
    async (req, res) => {
      const buffer = await sharp(req.file.buffer)
          .resize({width: 250, height: 250})
          .png()
          .toBuffer();
      req.user.photo = buffer;
      await req.user.save();
      res.status(200).json({message: 'Picture is uploaded!'});
    },
    (error, req, res, next) => {
      res.status(400).send({error: error.message});
    },
);

module.exports = router;
