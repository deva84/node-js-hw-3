const express = require('express');
const router = new express.Router();

const auth = require('../middlewares/auth');
const {Truck} = require('../models/truckModel');

router.get('/', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'DRIVER') {
      try {
        await user
            .populate({
              path: 'trucks',
            })
            .execPopulate();
        res.status(200).json({trucks: [...req.user.trucks]});
      } catch (e) {
        res.status(400).json({message: e.message});
      }
    } else {
      res.status(400).json({message: 'You are not driver'});
    }
  } else {
    res.status(400).json({message: 'You should log in'});
  }
});
router.post('/', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'DRIVER') {
      const truck = new Truck({
        created_by: req.user._id,
        type: req.body.type,
      });
      try {
        await truck.save();
        res.status(200).json({message: 'Truck has been created'});
      } catch (e) {
        res.status(400).json({message: e.message});
      }
    } else {
      res.status(400).json({message: 'You are not driver'});
    }
  } else {
    res.status(400).json({message: 'You should log in'});
  }
});

router.get('/:id', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'DRIVER') {
      const _id = req.params.id;

      const truck = await Truck.findOne({_id, created_by: req.user._id});

      if (!truck) {
        res.status(400).json({message: 'No such truck'});
      }
      if (!truck.assigned_to) {
        res.status(200).json({truck: truck});
      } else {
        res.status(400).json({message: 'This truck is assigned to different driver'});
      }
    } else {
      res.status(400).json({message: 'You are not driver'});
    }
  } else {
    res.status(400).json({message: 'You should log in'});
  }
});

router.put('/:id', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'DRIVER') {
      const _id = req.params.id;

      const truck = await Truck.findOne({_id, created_by: req.user._id});

      if (!truck) {
        res.status(400).json({message: 'No such truck'});
      }
      if (!truck.assigned_to) {
        try {
          truck.type = req.body.type;
          await truck.save();
          res
              .status(200)
              .json({message: 'Truck details were updated'});
        } catch (e) {
          res.status(400).json({message: e.message});
        }
      } else {
        res.status(400).json({
          message: 'Try later',
        });
      }
    } else {
      res.status(400).json({message: 'You are not driver'});
    }
  } else {
    res.status(400).json({message: 'You should log in'});
  }
});

router.delete('/:id', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'DRIVER') {
      const _id = req.params.id;

      const truck = await Truck.findOne({_id, created_by: req.user._id});

      if (!truck) {
        res.status(400).json({message: 'No such truck'});
      }
      if (!truck.assigned_to) {
        try {
          await Truck.findOneAndDelete({
            _id,
            created_by: req.user._id,
          });
          res.status(200).json({message: 'Truck has been deleted'});
        } catch (e) {
          res.status(400).json({message: e.message});
        }
      } else {
        res.status(400).json({
          message: 'Try later',
        });
      }
    } else {
      res.status(400).json({message: 'You are not driver'});
    }
  } else {
    res.status(400).json({message: 'You should log in'});
  }
});

router.post('/:id/assign', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'DRIVER') {
      const _id = req.params.id;

      const truck = await Truck.findOne({_id, created_by: req.user._id});

      if (!truck) {
        res.status(400).json({message: 'No such truck'});
      }
      try {
        truck.assigned_to = req.user._id;
        await truck.save();
        res.status(200).json({message: 'Truck has been assigned'});
      } catch (e) {
        res.status(400).json({message: e.message});
      }
    } else {
      res.status(400).json({message: 'You are not driver'});
    }
  } else {
    res.status(400).json({message: 'You should log in'});
  }
});

module.exports = router;
