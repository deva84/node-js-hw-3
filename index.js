require('dotenv').config();
const port = process.env.PORT || 8080;
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const loadRouter = require('./routers/loadRouter');
const truckRouter = require('./routers/truckRouter');

app.use(express.json());
app.use(express.static('build'));
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);


app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect(
        'mongodb+srv://test:hjlbyf1@cluster0.kfbem.mongodb.net/test?retryWrites=true&w=majority',
        {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
          useCreateIndex: true,
        },
    );

  } catch (e) {
    res.status(500).json({message: e.message});
  }

  app.listen(port, () => {
    console.log(`Server is workin at port ${port}...`);
  });
};

start();
